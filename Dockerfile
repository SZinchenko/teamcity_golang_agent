FROM java
# Dockerfile author / maintainer 
MAINTAINER Sergei Zinchenko <zinchenkosv@gmail.com> 

RUN   \
  apt-get update && \
  apt-get install -qqy git build-essential curl wget unzip apt-transport-https ca-certificates lxc iptables && \
  rm -rf /var/lib/apt/lists/*

RUN curl -o /usr/local/go1.9.2.linux-amd64.tar.gz https://storage.googleapis.com/golang/go1.9.2.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf /usr/local/go1.9.2.linux-amd64.tar.gz
RUN cd /usr/local/bin && ln -s /usr/local/go/bin/go

RUN curl -o /usr/local/bin/wrapdocker https://raw.githubusercontent.com/jpetazzo/dind/master/wrapdocker
RUN chmod +x /usr/local/bin/wrapdocker

RUN curl -sSL https://get.docker.com | sh

VOLUME /var/lib/docker

ADD setup.sh /setup.sh

RUN adduser --quiet teamcity
RUN mkdir -p /agent
RUN chown teamcity.teamcity /agent

RUN go get -v github.com/btcsuite/btcd


EXPOSE 9090
CMD TEAMCITY_SERVER=$TEAMCITY_SERVER bash /setup.sh run